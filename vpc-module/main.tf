module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = ">= 5.1.0"

  name              = "${local.project_name}-${local.environment}"
  cidr              = local.vpc_cidr

  azs               = local.azs 
  private_subnets   = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 4, k)]
  public_subnets    = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 48)]

  private_subnet_names = ["Private Subnet One", "Private Subnet Two"]

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
  }

  tags = local.tags
}

