variable "vpc_cidr" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "project_name" {
  description = "Project name can be the site name or the client name"
  type        = string
  default     = "Drupal-website"
}

variable "environment" {
  description = "The type of environement to be deployed eg. production, staging"
  type        = string
  default     = "staging"
}

variable "az_coverage" {
  description = "Number of availability zones coverage, indicates also how many subnets"
  type        = number
  default     = 3

  validation {
    condition     = var.az_coverage >= 1 && var.az_coverage <= 3
    error_message = "The number of availibity zones must be between 1 and 3."
  }
}
variable "private_subnets" {
  description = "List of private subnet IDs for the EKS cluster."
  type        = list(string)
}
