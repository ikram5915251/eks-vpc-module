locals {
  project_name  = var.project_name
  environment   = var.environment
  vpc_cidr      = var.vpc_cidr
  azs           = slice(data.aws_availability_zones.available.names, 0, var.az_coverage)

  tags = {
    project     = local.project_name
    environment = local.environment
    iac         = "true"
  }
}

data "aws_availability_zones" "available" {}