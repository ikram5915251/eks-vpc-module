# Inside rds-module/variables.tf

variable "vpc_id" {
  description = "The VPC ID where the RDS instance will be created."
  type        = string
}

variable "private_subnets" {
  description = "List of private subnet IDs for the RDS instance."
  type        = list(string)
}
variable "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  type        = string
}