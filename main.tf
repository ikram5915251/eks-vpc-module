provider "aws" {
    region = "us-east-1"
}
module "vpc" {
  source = "./vpc-module"
  # Plus any required variables for your VPC module
  private_subnets = module.vpc.private_subnets
}
#module "rds" {
  #source = "./rds-module"
  
  #vpc_id       = module.vpc.vpc_id
  #private_subnets = module.vpc.private_subnets
  # Any additional variables needed by your RDS module
  #vpc_cidr_block  = module.vpc.vpc_cidr_block # Passing VPC CIDR block to RDS module
# }
module "eks" {
  source  = "./eks-module"
  vpc_id       = module.vpc.vpc_id
  vpc_private_subnets_id = module.vpc.private_subnets
  cluster_node_size = 1
}

module "ecr" {
  source = "lgallard/ecr/aws"

  name                 = "ecr-repo-dev91834"
  scan_on_push         = true
  timeouts_delete      = "60m"
  image_tag_mutability = "MUTABLE"


  # Note that currently only one policy may be applied to a repository.
  policy = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "repo policy",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
      ]
}
EOF

  # Only one lifecycle policy can be used per repository.
  # To apply multiple rules, combined them in one policy JSON.
  lifecycle_policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire untagged images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        },
        {
            "rulePriority": 2,
            "description": "Keep last 30 dev images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["dev"],
                "countType": "imageCountMoreThan",
                "countNumber": 30
            },
            "action": {
                "type": "expire"
            }
        }
    ]
  }
EOF

  # Tags
  tags = {
    Owner       = "DevOps team"
    Environment = "dev"
    Terraform   = true
  }
}